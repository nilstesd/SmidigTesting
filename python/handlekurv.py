class Handlekurv:
    
    produkter = []
    totalPris = 0;
    
    def leggTil(self, produkt, pris):
        self.totalPris += pris
        self.produkter.append(produkt)
        
    def hentTotalPris(self):
        return self.totalPris;
    
    def hentAntallVarer(self):
        return len(self.produkter);
    
    def hentProdukt(self, indeks):
        return self.produkter[indeks];
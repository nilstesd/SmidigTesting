from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--headless")
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://10.24.91.89:8080/NotSoSecureBank")
time.sleep(1)
assert "Logg inn i banken" in driver.title
#time.sleep(1)
elem = driver.find_element_by_id("inputEmail")
elem.send_keys("apekatt@mikkemus.no")
elem = driver.find_element_by_id("inputPassword")
elem.send_keys("fredrikerkul")
#time.sleep(1)
elem = driver.find_element_by_class_name("btn")
elem.click()
time.sleep(1)
assert "IkkeSåSikkerBanken" in driver.title
time.sleep(1)
elem = driver.find_element_by_id("firstName")
elem.clear()
elem.send_keys("Pål")
elem = driver.find_element_by_id("lastName")
elem.clear()
elem.send_keys("Tesdal")
elem = driver.find_element_by_class_name("btn")
elem.click()
time.sleep(1)
elem = driver.find_element_by_link_text("Konto")
elem.click()
time.sleep(1)
elem = driver.find_element_by_id("balance")
SaldoFør = float(elem.text)
elem = driver.find_element_by_link_text("Betaling")
elem.click()
time.sleep(1)
elem = driver.find_element_by_id("email")
elem.send_keys("sol@regn.no")
elem = driver.find_element_by_id("description")
elem.send_keys("Oppgjør for i går")
elem = driver.find_element_by_id("amount")
Transaksjon = 500
elem.send_keys(str(Transaksjon))
elem = driver.find_element_by_class_name("btn")
time.sleep(3)
elem.click()
time.sleep(1)
assert "Transaksjoner" in driver.page_source
elem = driver.find_element_by_id("balance")
SaldoEtter = float(elem.text)
print("Saldo Før: " + str(SaldoFør))
print("Saldo Etter: " + str(SaldoEtter))
forventet = SaldoFør - Transaksjon
print("Forventet: " + str(forventet))
print("Transaksjonsbeløp: " + str(Transaksjon))
assert SaldoEtter == forventet
elem = driver.find_element_by_id("logout")
elem.click()
time.sleep(2)

driver.close()
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options

@given(u'at jeg har logget meg på NotSoSecureBank og valgt å se på konto')
def step_impl(context):
	options = Options()
	options.add_argument("--headless")
	context.driver = webdriver.Chrome(chrome_options=options)
	context.driver.get("http://bigdata.stud.iie.ntnu.no/NotSoSecureBank/")
	time.sleep(1)
	assert "Logg inn i banken" in context.driver.title
	
	elem = context.driver.find_element_by_id("inputEmail")
	elem.send_keys("gunder@gundersen.no")
	elem = context.driver.find_element_by_id("inputPassword")
	elem.send_keys("gunder")
	time.sleep(2)
	elem = context.driver.find_element_by_class_name("btn")
	elem.click()
	time.sleep(1)
	assert "IkkeSåSikkerBanken" in context.driver.title
	time.sleep(1)
	elem = context.driver.find_element_by_link_text("Konto")
	elem.click()
	time.sleep(2)
	elem = context.driver.find_element_by_id("balance")
	context.saldoFør = float(elem.text)
	
@when(u'jeg velger betaling')
def step_impl(context):
	elem = context.driver.find_element_by_link_text("Betaling")
	elem.click()
	time.sleep(2)

@when(u'legger inn mottaker og sum og trykker betal')
def step_impl(context):
	elem = context.driver.find_element_by_id("email")
	elem.send_keys("sol@regn.no")
	elem = context.driver.find_element_by_id("description")
	elem.send_keys("Oppgjør for i går")
	elem = context.driver.find_element_by_id("amount")
	context.transaksjon = 500
	elem.send_keys(str(context.transaksjon))
	elem = context.driver.find_element_by_class_name("btn")
	time.sleep(3)
	elem.click()
	time.sleep(3)

@then(u'skal jeg få se transaksjonshistorikken')
def step_impl(context):
	assert "Transaksjoner" in context.driver.page_source

@then(u'saldoen skal ha minsket med summen i betalingen')
def step_impl(context):
	elem = context.driver.find_element_by_id("balance")
	context.saldoEtter = float(elem.text)
	forventet = context.saldoFør - context.transaksjon
	assert context.saldoEtter == forventet
	elem = context.driver.find_element_by_id("logout")
	elem.click()

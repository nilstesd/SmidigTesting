Egenskap: Betaling
Som kunde
Ønsker jeg å legge inn betaling
Så den jeg skylder får pengene sine

Scenario: legg inn betaling
Gitt at jeg har logget meg på NotSoSecureBank og valgt å se på konto
Når jeg velger Betaling
Og legger inn mottaker og sum og trykker Betal
Så skal jeg få se transaksjonshistorikken
Og saldoen skal ha minsket med summen i betalingen
